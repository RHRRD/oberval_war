package world

class Russia(private var rockets: Long) : Country {

    override fun sendRocket() {
        if (rockets <= 0) {
            println("Russian does not have a rocket...")
        } else {
            println("Russia send a rocket!")
            rockets--
        }
    }

    override fun deleteRocket() {
        if (this.rockets > 0) {
            this.rockets--
            println("Russia remove one rocket. AllRockets: $rockets")
        }
    }

    override fun addRocket() {
        this.rockets++
        println("Russia add one rocket. AllRockets: $rockets")
    }

}
