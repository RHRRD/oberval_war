package world

class Earth: World {

    private var countries = mutableListOf<Country>()

    override fun addCountry(country: Country) {
        println("United Nations: ${country.javaClass}, welcome to UN!")
        this.countries.add(country)
        println()
    }

    override fun deleteCountry(country: Country) {
        println("United Nations: ${country.javaClass} remove from UN.")
        this.countries.remove(country)
        println()
    }

    override fun notifyAboutSendRockets() {
        println("United Nations: Disarmament policy : nuclear war")
        this.countries.forEach { it.sendRocket() }
        println()
    }

    override fun notifyAboutDeleteRocket() {
        println("United Nations: Disarmament policy : on")
        this.countries.forEach { it.deleteRocket() }
        println()
    }

    override fun notifyAboutAddRocket() {
        println("United Nations: Disarmament policy : off")
        this.countries.forEach { it.addRocket() }
        println()
    }

}