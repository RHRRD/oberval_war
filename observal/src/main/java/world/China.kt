package world

class China(private var rockets: Long): Country {

    override fun sendRocket() {
        if (this.rockets <= 0) {
            println("China does not have a rocket...")
        } else {
            println("China send a rocket!")
            this.rockets--
        }
    }

    override fun deleteRocket() {
        if (this.rockets > 0) {
            this.rockets--
            println("China remove one rocket. AllRockets: $rockets")
        }
    }

    override fun addRocket() {
        this.rockets++
        println("China add one rocket. AllRockets: $rockets")
    }

}
