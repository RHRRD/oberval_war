package world

class NorthKorea(private var rockets: Long): Country {

    override fun sendRocket() {
        if (this.rockets <= 0) {
            println("North Korea does not have a rocket...")
        } else {
            do {
                println("North Korea send a rocket!")
                this.rockets--
            } while (this.rockets > 0)
        }
    }

    override fun deleteRocket() {
        println("North Korea remove one rocket. AllRockets: $rockets")
    }

    override fun addRocket() {
        this.rockets += 2
        println("North Korea add one rocket. AllRockets: $rockets")
    }

}