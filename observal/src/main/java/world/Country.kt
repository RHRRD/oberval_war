package world

interface Country {

    fun sendRocket()

    fun deleteRocket()

    fun addRocket()

}