package world

interface World {

    fun addCountry(country: Country)

    fun deleteCountry(country: Country)

    fun notifyAboutSendRockets()

    fun notifyAboutDeleteRocket()

    fun notifyAboutAddRocket()

}