package world

class USA(private var rockets: Long): Country {

    override fun sendRocket() {
        if (this.rockets <= 0) {
            println("USA does not have a rocket...")
        } else {
            println("USA send a rocket!")
            this.rockets--
        }
    }

    override fun deleteRocket() {
        if (this.rockets > 0) {
            this.rockets--
            println("USA remove one rocket. AllRockets: $rockets")
        }
    }

    override fun addRocket() {
        this.rockets++
        println("USA add one rocket. AllRockets: $rockets")
    }

}