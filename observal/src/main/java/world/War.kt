package world

class War {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            val russia = Russia(5)
            val usa = USA(5)
            val china = China(3)
            val northKorea = NorthKorea(1)
            val allEarth = Earth()
            allEarth.addCountry(russia)
            allEarth.addCountry(usa)
            allEarth.addCountry(china)
            allEarth.addCountry(northKorea)

            allEarth.notifyAboutDeleteRocket()
            allEarth.notifyAboutDeleteRocket()
            allEarth.notifyAboutDeleteRocket()

            northKorea.addRocket()
            allEarth.deleteCountry(northKorea)
            allEarth.notifyAboutAddRocket()

            northKorea.sendRocket()
            allEarth.notifyAboutSendRockets()
            allEarth.notifyAboutSendRockets()
        }

    }
}